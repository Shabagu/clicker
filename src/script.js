let clicks = {
  value: 0,
  view: document.getElementsByClassName('counter')[0],
}

function click_() {
  clicks.value++;
  clicksUpdate(0);
}

function restart() {
  clicks.value = 0;
  sounds.restart.play();
  clicksUpdate(700);
}

function clicksUpdate(delay) {
  setTimeout( function() {
    clicks.view.textContent = clicks.value;
  }, delay);
}
