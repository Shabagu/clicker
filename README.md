# Clicker


### What is it?
...Just a simple project to learn basics of git. Nothing serious.

### Why it's a clicker?
...idk, it's the first thing that came to my mind.  
...I hope that project's simplicity will not complicate my further studying

### Plans for the project
..If the development of such a project turns out to be interesting, I'll deal with it  
..Clicker is a good idea for a mobile app =)

### Technology stack
..I feel like I don't need to use SPA frameworks at the beginning, for avoid overengineering, so I went "naked" JS.
